import requests
import datetime

# crontab
# 0     3 *  *  * source /<PATH_TO_ENV>/bin/activate; /<PATH_TO_ENV>/bin/python /<PATH_TO_PROJECT>/crons/curl_most_used_cities.py >> /<PATH_TO_LOGS>/bestcities.log 2>&1; deactivate

airports = [
    'AMS',
    'ARN',
    'BCN',
    'BGY',
    'BOS',
    'BRU',
    'BVA',
    'CDG',
    'CGN',
    'CRL',
    'DFW',
    'DUB',
    'DXB',
    'FCO',
    'FRA',
    'GVA',
    'HAM',
    'IST',
    'LAX',
    'LHR',
    'LIN',
    'LIS',
    'LTN',
    'MAD',
    'MIA',
    'MUC',
    'MXP',
    'ORY',
    'OTP',
    'PEK',
    'PRG',
    'VIE',
    'WAW',
    'ZRH',
]

for airport in airports:
    url = 'http://iflytomorrow.com/city/{}-sky/?json=1'.format(airport)

    response = requests.get(url)
    ok = "Not OK"
    if response.status_code == 200:
        ok = "OK"

    when = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("{} Getting {}... {}".format(when, airport, ok))
