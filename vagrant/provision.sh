#!/bin/bash
apt-get update

# install mysql
export DEBIAN_FRONTEND=noninteractive
apt-get -q -y install mysql-server-5.6
echo "Give mysql server time to start up before we try to set a password..."
sleep 5
mysql -uroot -e <<EOSQL "UPDATE mysql.user SET Password=PASSWORD('vagrant') WHERE User='root'; FLUSH PRIVILEGES;"
EOSQL

mysql -uroot -pvagrant -e <<EOSQL "CREATE DATABASE flights;"
EOSQL

mysql -uroot -pvagrant -Dflights < /vagrant/vagrant/sql/flights.sql

# END mysql install
