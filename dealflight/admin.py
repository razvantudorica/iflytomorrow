from django.contrib import admin

from .models import Subscriber, Article, Tag


class MembershipInline(admin.TabularInline):
    model = Article.tags.through


class PersonAdmin(admin.ModelAdmin):
    inlines = [
        MembershipInline,
    ]


class GroupAdmin(admin.ModelAdmin):
    inlines = [
        MembershipInline,
    ]
    exclude = ('tags',)


class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

admin.site.register(Subscriber)
admin.site.register(Article)
admin.site.register(Tag)