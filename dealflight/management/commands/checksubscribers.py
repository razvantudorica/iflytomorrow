from django.core.management.base import BaseCommand, CommandError
from dealflight.models import Subscriber
from dealflight.views import get_start_end_dates
from django.conf import settings

class Command(BaseCommand):
    help = 'Checks flights and sends emails to subscribers'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        subscribers = Subscriber.objects.filter(confirmed=True)
        start_date, end_date = get_start_end_dates()

        for subscriber in subscribers:

            url = '{url}/browsequotes/' \
                  'v1.0/DE/{currency}/en-US/{airport}/anywhere/{date_start}/' \
                  '{date_return}?apiKey={api_key}'.format(
                url=settings.SKYSCANNER_BASE_URL,
                currency=settings.CURRENCY,
                airport=subscriber.airport_code,
                date_start=start_date,
                date_return=end_date,
                api_key=settings.SKYSCANNER_KEY
            )

            url = url.encode('utf8')

            self.stdout.write(self.style.SUCCESS(url))

        self.stdout.write(self.style.SUCCESS('Successfully send email'))