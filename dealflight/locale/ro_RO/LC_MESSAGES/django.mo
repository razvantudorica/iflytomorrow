��    $      <  5   \      0     1     7  +   @  
   l     w          �     �  	   �     �     �     �     �     �            (     (   D     m  #   z     �  *   �     �     �  %   �  �   #  *   �     �     �  �      X   �  
   �     �     �     �  @    
   C     N  *   W     �     �     �     �     �     �     �     
	     	     2	     C	     I	     f	  (   t	  6   �	     �	  (   �	     
  (    
     I
     Z
  %   k
  �   �
  4   *     _     o  �   �  T   ;     �     �     �     �                     $                                   	                                 "         #          !                           
                                             About Articles Articles, news and blog posts about flights Buy it for Buy now Change Change my city Cheapest flight for tomorrow City page Confirm subscription Email address Flight not found. Fly from... From Have a nice flight! Home Let me know when there is something else Most of our pages have a JSON view also. Next Flights No flight found from your location. Subscribe me! Thank you for confirming your subscription The flight for tomorrow The flight to This flight is not available anymore. This website shows only one flight. <strong>The cheapest flight</strong> you can get tomorrow and return back in two days. Nothing more. Nothing less. To get the first page in JSON format, call Too late... Type your city We do not sell tickets and we do not offer any kind of support. We just find for you <strong>the cheapest flight for tomorrow to ANYWHERE</strong>! Write down your email address and we'll send you an email when we find a flight for you. and return for on to Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-17 23:42+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Despre noi Articole Articole, știri si bloguri despre zboruri Cumpără-l pentru Cumpără acum Schimbă Schimbă orașul de plecare Cel mai ieftin zbor mâine Pagina de oraș Confirmă înscrierea Adresa de email Zborul nu a fost găsit Zboară de la... De la Îți urăm un zbor plăcut! Prima pagină Anunță-mă când se găsește alt zbor Majoritatea paginilor noastre sunt și în format JSON Zborurile următoare Nu a fost găsit niciun zbor în zona ta Înscrie-mă! Mulțumim că ați confirmat înscrierea Zborul de mâine Zborul de mâine Acest zbor nu mai este de actualitate Pe acest site vei găsi un singur zbor. <strong>Cel mai ieftin zbor</strong> pe care îl poți lua mâine cu întoarcere în două zile. Nimic mai mult. Pentru a lua prima pagină în format JSON apelează Prea târziu... Scrie numele orașului Acest site nu vinde bilete de avion și nu oferă nici un fel de suport pentru cumpărarea de bilete. Doar caută <strong>cel mai ieftin zbor pentru mâine către ORIUNDE</strong>! Scrie-ți adresa de email iar noi te vom anunța când vom găsi un zbor pentru tine întoarcerea pentru pe către 