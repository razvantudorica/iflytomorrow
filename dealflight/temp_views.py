import logging
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseBadRequest
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from .models import Subscriber

# Get an instance of a logger
logger = logging.getLogger('flights')


def api_info(request):
    return render(request, 'api_info.html')


@csrf_exempt
def subscribe(request):
    if not request.is_ajax():
        return HttpResponseBadRequest()

    email = request.POST.get('email', '')
    try:
        max_price = int(request.POST.get('max', 0))
    except ValueError:
        max_price = 0

    city_code = request.POST.get('city', '')

    if max_price <= 0 or len(city_code) < 3:
        print("Invalid max, city:", max_price, city_code)
        return HttpResponseBadRequest()

    try:
        validate_email(email)
    except ValidationError:
        print("Invalid email:", email)
        return HttpResponseBadRequest()

    subscriber = Subscriber(airport_code=city_code,
                            email=email,
                            max_price=max_price,)

    try:
        subscriber.save()
        subscriber.send_email(email)
    except IntegrityError:
        logger.error("The email/city combination already subscribed")
    except Exception as ex:
        logger.error("Other error: {}".format(ex))

    return JsonResponse({'ok': True})


def confirm(request, key):
    try:
        subscriber = Subscriber.objects.get(key=key)
    except Subscriber.DoesNotExist:
        raise Http404("Key not found")

    subscriber.key = ''
    subscriber.confirmed = True
    subscriber.save()

    return render(request, 'confirm.html', {})
