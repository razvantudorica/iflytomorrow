import os
import logging

from ipaddress import ip_address

from geoip2.errors import AddressNotFoundError
from geoip2.database import Reader
from django.conf import settings

from dealflight.exceptions import GeoIP2Exception


logger = logging.getLogger('flights')


GEOIP_SETTINGS = {
    'GEOIP_PATH': getattr(settings, 'GEOIP_PATH', None),
    'GEOIP_CITY': getattr(settings, 'GEOIP_CITY', 'GeoLite2-City.mmdb'),
    'GEOIP_COUNTRY': getattr(settings, 'GEOIP_COUNTRY', 'GeoLite2-Country.mmdb'),
}

def get_client_ip(request):
    if settings.DEBUG:
        return '83.103.169.132'
        return '95.211.186.129'

        ips = ['188.26.179.63', '95.211.186.129', '5.57.20.8', '50.22.90.226', '217.70.180.226', '216.172.168.37',
               '91.199.104.6', '86.126.138.214']
        from random import randint
        rnd = randint(0, len(ips) - 1)
        print(ips[rnd])
        return ips[rnd]

    ip = request.META.get('REMOTE_ADDR', None)
    if ip != '':
        addr = ip_address(ip)
        if not addr.is_private:
            return ip

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
        addr = ip_address(ip)
        if not addr.is_private:
            return ip
    else:
        ip = request.META.get('REMOTE_ADDR')

    if not ip:
        ip = '127.0.0.1'

    return ip


def get_geo_location(request):

    path = GEOIP_SETTINGS['GEOIP_PATH']
    if not path:
        raise GeoIP2Exception('GeoIP path must be provided via '
                              'the GEOIP_PATH setting.')

    ip = get_client_ip(request)

    iso_code = 'RO'
    latitude, longitude = 25.0, 46.0
    precision = 1

    reader = Reader(os.path.join(path, GEOIP_SETTINGS['GEOIP_CITY']))
    try:
        city = reader.city(ip)
        latitude, longitude = city.location.latitude, city.location.longitude
        latitude = int(latitude * 10**precision) / 10.0**precision
        longitude = int(longitude * 10**precision) / 10.0**precision
        iso_code = city.country.iso_code
    except AddressNotFoundError:
        logger.error("Could not find IP: {} in GeoIP2 db".format(ip))

    return {
        'iso_code': iso_code,
        'ip': ip,
        'latitude': latitude,
        'longitude': longitude
    }
