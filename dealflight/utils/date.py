from arrow import utcnow
from django.conf import settings


def get_start_end_dates():
    timezone_diff = settings.BUCHAREST_ZONE
    utc = utcnow()
    local = utc.replace(hours=+timezone_diff)
    tomorrow = local.replace(days=+1)
    end_trip = tomorrow.replace(days=+2)
    start_date = "{year}-{month}-{day}".format(year=tomorrow.year,
                                               month=str(tomorrow.month).zfill(2),
                                               day=str(tomorrow.day).zfill(2))
    end_date = "{year}-{month}-{day}".format(year=end_trip.year,
                                             month=str(end_trip.month).zfill(2),
                                             day=str(end_trip.day).zfill(2))

    return start_date, end_date