from hashlib import md5 as md5sum


def md5(string):
    return md5sum(string).hexdigest()