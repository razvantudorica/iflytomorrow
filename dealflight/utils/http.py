import logging
import json
import requests

from datetime import datetime, timedelta

from django.conf import settings
from django.http import HttpResponseServerError
from django.core.cache import cache

from dealflight.utils.crypto import md5
# Get an instance of a logger
logger = logging.getLogger(__name__)


def do_api_request(url, no_cache=False, store_cache=True, cache_ttl=None):
    cache_key = md5(url.encode('utf-8'))

    if no_cache is False:
        data = cache.get(cache_key, None)
        if data:
            return json.loads(data)

    logger.debug("Call url: {}".format(url))
    response = requests.get(url)
    if response.status_code != requests.codes.ok:
        logger.error("SKYSCANNER ERROR. Could not get {}, {}, {}".format(url, response.status_code, response.text))
        return HttpResponseServerError("Could not fetch prices")

    data = response.json()

    if cache_ttl is None:
        cache_ttl = settings.CACHE_TTL

    if store_cache:
        cache.set(cache_key, json.dumps(data), cache_ttl)

    return data


def set_cookie(response, name, value):
    days_expire = 7
    max_age = days_expire * 24 * 60 * 60
    expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    response.set_cookie(name, value,  max_age=max_age, expires=expires)

