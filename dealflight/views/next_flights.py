import logging
from datetime import datetime

from django.views import View
from django.shortcuts import render

from dealflight.models import Flight

# Get an instance of a logger
logger = logging.getLogger('flights')


class NextFlightsView(View):
    template_name = 'next_flights.html'

    def get(self, request, *args, **kwargs):
        data = {}
        today = datetime.today()
        data['flights'] = Flight.objects.filter(start_date__gte=today).order_by('-start_date')

        return render(request, self.template_name, data)

