import logging
import time
import os
from datetime import datetime

from django.conf import settings
from django.views import View
from django.http import HttpResponse

from dealflight.models import Flight

# Get an instance of a logger
logger = logging.getLogger('flights')


class SitemapView(View):
    template_name = 'next_flights.html'

    def get(self, request, *args, **kwargs):
        sitemap_path = os.path.join(os.path.dirname(__file__)) + "/../static/sitemap.xml"
        last_change = os.path.getmtime(sitemap_path)
        diff = time.time() - last_change
        if diff <= settings.SITEMAP_CACHE:
            # return this one
            with open(sitemap_path, "r") as sitemap:
                data = sitemap.readlines()

            return HttpResponse(data, content_type='text/xml; charset=UTF-8')

        header_path = os.path.join(os.path.dirname(__file__)) + "/../static/header_sitemap.txt"
        header_xml = ''
        with open(header_path, "r") as header:
            header_xml = header.read()

        hostname = "{protocol}://{domain}".format(protocol=settings.PROTOCOL, domain=settings.DOMAIN)

        header_xml = header_xml.replace('{HOSTNAME}', hostname)
        sitemap_data = header_xml + "\n"
        urls = [('/', 'hourly', 1), ('/next-flights/', 'hourly', 0.8), ('/about/', 'yearly', 0.4)]

        today = datetime.today()
        flights = Flight.objects.filter(start_date__gte=today)
        for f in flights:
            flight_url = ('/flight/{}/'.format(f.perm_url), 'hourly', 0.7)
            urls.append(flight_url)

        for url_data in urls:
            sitemap_data += '<url>\n <loc>{hostname}{url}</loc>\n'.format(hostname=hostname, url=url_data[0])
            sitemap_data += ' <changefreq>{}</changefreq>\n'.format(url_data[1])
            sitemap_data += ' <priority>{}</priority>\n'.format(url_data[2])
            sitemap_data += '</url>\n'

        sitemap_data += '</urlset>'
        with open(sitemap_path, "w") as sitemap:
            sitemap.write(sitemap_data)

        return HttpResponse(sitemap_data, content_type='text/xml; charset=UTF-8')

