import logging
from django.http import JsonResponse
from django.views import View
from django.shortcuts import render

from dealflight.exceptions import NoQuoteException
from dealflight.utils.date import get_start_end_dates
from dealflight.utils.geo import get_geo_location
from dealflight.skyscanner import get_cheapest_flight
from dealflight.views.city import CityView

# Get an instance of a logger
logger = logging.getLogger('flights')


class IndexView(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        city_id = request.COOKIES.get('city_id', None)

        if city_id:
            return CityView.as_view()(request, city_id=city_id)

        as_json = request.GET.get('json', False)
        no_cache = request.GET.get('no-cache', False)
        geo = get_geo_location(request)
        start_date, _ = get_start_end_dates()

        try:
            logger.error("1st call")
            response_data = get_cheapest_flight(geo, no_cache=no_cache)
        except NoQuoteException:
            logger.error("No quote... try 2nd way")
            try:
                response_data = get_cheapest_flight(geo, guess_city=True, no_cache=no_cache)
            except NoQuoteException:
                logger.error("NoQuoteException 2")
                return render(request, 'index_noquote.html')
        except Exception as ex:
            logger.error("Could not get response: {}".format(ex))
            return render(request, 'index_noquote.html')

        if as_json:
            return JsonResponse(response_data)

        beta = request.GET.get('beta', False)
        if beta:
            response_data['beta'] = 1

        return render(request, self.template_name, response_data)

