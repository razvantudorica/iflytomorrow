from datetime import datetime

from django.conf import settings
from django.db import IntegrityError
from django.views import View
from django.utils.translation import get_language
from django.http import (JsonResponse,
                         HttpResponseServerError,
                         HttpResponseBadRequest)

from dealflight.models import Flight
from dealflight.utils.http import do_api_request


class CheckPriceView(View):

    def get(self, request, *args, **kwargs):

        if not request.is_ajax():
            return HttpResponseBadRequest()

        flight_id = kwargs.get('flight_id', None)

        try:
            fl = Flight.objects.get(perm_url=flight_id)
        except Flight.DoesNotExist:
            return JsonResponse({'quote': False, 'min_price': -1})

        country_code = 'DE'
        currency = settings.CURRENCY
        locale = get_language()

        url = '{baseurl}/browsequotes/v1.0/{country_code}/{currency}/{locale}/{start}/{end}/{start_date}/' \
              '{end_date}?apiKey={api_key}'.format(baseurl=settings.SKYSCANNER_BASE_URL,
                                                   country_code=country_code,
                                                   currency=currency,
                                                   locale=locale,
                                                   start=fl.airport_start.iata_code,
                                                   end=fl.airport_end.iata_code,
                                                   start_date=fl.start_date.strftime('%Y-%m-%d'),
                                                   end_date=fl.return_date.strftime('%Y-%m-%d'),
                                                   api_key=settings.SKYSCANNER_KEY)

        data = do_api_request(url, cache_ttl=settings.CACHE_TTL)

        if isinstance(data, HttpResponseServerError):
            return JsonResponse({'quote': False, 'min_price': -1})

        quotes = data['Quotes']
        if len(quotes):
            min_quote = quotes[0]
        else:
            return JsonResponse({'quote': False, 'min_price': 0})

        for quote in quotes:
            if not quote['Direct']:
                continue

            if quote['MinPrice'] < min_quote['MinPrice']:
                min_quote = quote

        price = min_quote['MinPrice']

        fl.price = price
        fl.last_update = datetime.now().replace(tzinfo=None)
        try:
            fl.save()
        except IntegrityError:
            # todo: some magic to not keep the url
            fl.delete()

        return JsonResponse({'quote': True, 'min_price': price, 'quote_date_time': min_quote['QuoteDateTime']})