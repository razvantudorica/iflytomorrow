import logging

from dealflight.utils.http import do_api_request
from dealflight.utils.date import get_start_end_dates
from dealflight.utils.http import set_cookie

from dealflight.skyscanner import get_response_data
from dealflight.exceptions import NoQuoteException
from dealflight.models import Flight


from django.urls import reverse
from django.conf import settings
from django.views import View
from django.http import (JsonResponse,
                         HttpResponseServerError)

from django.shortcuts import render

# Get an instance of a logger
logger = logging.getLogger('flights')


class CityView(View):

    def get(self, request, *args, **kwargs):
        """

        :param request: Http Request
        :type request: django.http.HttpRequest
        :param args:
        :param kwargs:
        :return:
        """
        city_id = kwargs.get('city_id', None)

        as_json = request.GET.get('json', False)
        baseurl = settings.SKYSCANNER_BASE_URL
        currency = settings.CURRENCY
        locale = 'en-US'

        url1 = '{baseurl}/autosuggest/v1.0/DE/{currency}/' \
               'en-US/?id={city_id}&apiKey={api_key}'.format(baseurl=baseurl,
                                                             city_id=city_id,
                                                             currency=currency,
                                                             api_key=settings.SKYSCANNER_KEY)
        data = do_api_request(url1)

        country_code = data['Places'][0]['CountryId'].replace('-sky', '')

        start_date, end_date = get_start_end_dates()

        url2 = '{baseurl}/browsequotes/v1.0/{country_code}/{currency}/{locale}/' \
               '{city_id}/anywhere/{start_date}/{end_date}?apiKey={api_key}'.format(baseurl=baseurl,
                                                                                    country_code=country_code,
                                                                                    currency=currency,
                                                                                    locale=locale,
                                                                                    city_id=city_id,
                                                                                    start_date=start_date,
                                                                                    end_date=end_date,
                                                                                    api_key=settings.SKYSCANNER_KEY)

        data = do_api_request(url2)

        try:
            response_data, airport1, airport2 = get_response_data(data=data, currency=settings.CURRENCY)
        except NoQuoteException:
            logger.error("No quote found for {} ".format(url2))
            return render(request, 'index_noquote.html')

        referral_url = '{baseurl}/referral/v1.0/{market}/{currency}/{locale}/' \
                       '{firstWaySkCodeFrom}/{firstWaySkCodeTo}/{startDate}/' \
                       '{endDate}?apiKey=' \
                       '{shortApiKey}'.format(baseurl=baseurl,
                                              market=country_code,
                                              currency=currency,
                                              locale=locale,
                                              firstWaySkCodeFrom=response_data['first_way']['skCodeFrom'],
                                              firstWaySkCodeTo=response_data['first_way']['skCodeTo'],
                                              startDate=start_date, endDate=end_date,
                                              shortApiKey=settings.SKYSCANNER_KEY[:16])

        response_data['buyUrl'] = referral_url

        fl = Flight.save_flight(airport1, airport2, response_data)
        response_data['permUrl'] = reverse('flight', args=[fl.perm_url, ])

        if as_json:
            return JsonResponse(response_data)

        beta = request.GET.get('beta', False)
        if beta:
            response_data['beta'] = 1

        response = render(request, 'index.html', response_data)
        set_cookie(response, 'city_id', city_id)

        return response


class SearchCityView(View):

    def get(self, request, *args, **kwargs):
        """

        :param request: Http Request
        :type request: django.http.HttpRequest
        :param args:
        :param kwargs:
        :return:
        """
        baseurl = settings.SKYSCANNER_BASE_URL
        currency = settings.CURRENCY
        locale = 'en-US'

        # change THIS!!
        country_code = request.GET.get('countryCode', 'DE')
        query = request.GET.get('query', None)

        if not query:
            return JsonResponse({})

        # http://partners.api.skyscanner.net/apiservices/autosuggest/v1.0/FR/EUR/en-US/?query=Par&apiKey=
        main_url = "{baseurl}/autosuggest/{version}/{country_code}/{currency}/{locale}/?query={query}".format(
            baseurl=baseurl,
            version=settings.SKYSCANNER_VERSION,
            country_code=country_code,
            currency=currency,
            locale=locale,
            query=query,
        )

        url = "{main_url}&apiKey={api_key}".format(
            main_url=main_url,
            api_key=settings.SKYSCANNER_KEY
        )

        data = do_api_request(url)
        if isinstance(data, HttpResponseServerError):
            return JsonResponse([], safe=False)

        response_data = list()
        for place in data.get('Places', []):
            label = "{placeName} ({countryName})".format(placeName=place['PlaceName'], countryName=place['CountryName'])

            response_data.append({'label': label, 'value': place['PlaceId']})

        return JsonResponse(response_data, safe=False)


