from dealflight.models import Article


from django.views import View
from django.http import Http404
from django.shortcuts import render, get_object_or_404


class ArticlesView(View):
    template_name = 'articles.html'

    def get(self, request, *args, **kwargs):
        page = kwargs.get('page', 1)

        # todo pagination
        posts = Article.objects.filter(status=Article.STATUS_PUBLISHED, locale=request.LANGUAGE_CODE).order_by(
            '-published_on')

        return render(request, self.template_name, {'articles': posts})

# def articles(request, page=None):
#     if not page:
#         page = 1
#
#     # todo pagination
#     posts = Article.objects.filter(status=Article.STATUS_PUBLISHED, locale=request.LANGUAGE_CODE).order_by('-published_on')
#     return render(request, 'articles.html', {'articles': posts})

class ArticleView(View):
    template_name = 'article.html'

    def get(self, request, *args, **kwargs):
        article_id = kwargs.get('article_id')
        slug = kwargs.get('slug')
        if not slug or not article_id:
            raise Http404("Wrong slug/id")

        post = get_object_or_404(Article, pk=article_id)
        if post.slug != slug:
            raise Http404("Wrong id")

        return render(request, self.template_name, {'article': post})

# def article(request, article_id, slug):
#     post = get_object_or_404(Article, pk=article_id)
#     if post.slug != slug:
#         raise Http404("Wrong id")
#
#     return render(request, 'article.html', {'article': post})
