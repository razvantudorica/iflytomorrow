import logging
from datetime import datetime

from django.views import View
from django.shortcuts import render

from dealflight.models import Flight

# Get an instance of a logger
logger = logging.getLogger('flights')


class FlightView(View):
    template_name = 'flight.html'

    def get(self, request, *args, **kwargs):
        perm_url = kwargs.get('perm_url', '')
        try:
            fl = Flight.objects.get(perm_url=perm_url)
        except Flight.DoesNotExist:
            return render(request, 'flight_404.html')

        expired = False
        if datetime.today() >= fl.start_date.replace(tzinfo=None):
            expired = True

        return render(request, self.template_name, {'flight': fl, 'expired': expired})

