import os

from django.views import View
from django.shortcuts import render


class AboutView(View):
    template_name = 'about.html'

    def get(self, request, *args, **kwargs):
        path = os.path.join(os.path.dirname(__file__), 'version.txt')
        try:
            f = open(path, 'r')
            ver = f.read()
        except:
            ver = ''

        return render(request, self.template_name,  {'version': ver})
