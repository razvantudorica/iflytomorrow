import time
import logging
import hashlib, binascii
from datetime import datetime

from django.db import models, IntegrityError
from django.conf import settings
from django.utils import timezone
from django.utils.text import slugify
from shortuuidfield import ShortUUIDField
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

logger = logging.getLogger('flights.models')


class Subscriber(models.Model):
    airport_code = models.CharField(max_length=10)
    email = models.EmailField()
    max_price = models.IntegerField()
    confirmed = models.BooleanField(default=False)
    created_on = models.DateTimeField(default=timezone.now())
    key = models.CharField(max_length=255, default='')

    def __str__(self):
        return "{} @ {}".format(self.email, self.airport_code)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.id is None:
            hash_key = '{}{}'.format(self.email, int(time.time()))
            dk = hashlib.pbkdf2_hmac('sha256',
                                     bytes(hash_key, 'utf-8'),
                                     bytes(settings.SALT_SUBSCRIBER, 'utf-8'),
                                     100000)

            self.key = binascii.hexlify(dk).decode("utf-8")

        super(Subscriber, self).save(force_insert, force_update, using, update_fields)

    def send_email(self, email):
        # TODO: do it async, celery or smthg

        server = "localhost"
        msg = MIMEMultipart()
        from_email = 'iFlyTomorrow <no-reply@iflytomorrow.com>'
        msg['From'] = from_email
        msg['To'] = email
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = 'Confirm iFlyTomorrow subscription'

        text = 'Please confirm your subscription to iFlyTomorrow. Click the link: http://iflytomorrow.com/confirm/{}'.format(self.key)

        msg.attach(MIMEText(text))
        smtp = smtplib.SMTP(server)
        smtp.sendmail(from_email, [email, ], msg.as_string())
        smtp.close()

    class Meta:
        unique_together = (('airport_code', 'email'), )


class Airport(models.Model):
    sky_id = models.IntegerField(unique=True)
    iata_code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=255)
    airport_type = models.CharField(max_length=255, default='Station')
    skyscanner_code = models.CharField(max_length=10, unique=True)
    city_name = models.CharField(max_length=255)
    city_id = models.CharField(max_length=10)
    country_name = models.CharField(max_length=255)

    @staticmethod
    def from_dict(data, save=True):
        airport = Airport()
        airport.airport_type = data['Type']
        airport.city_id = data['CityId']
        airport.skyscanner_code = data['SkyscannerCode']
        airport.name = data['Name']
        airport.iata_code = data['IataCode']
        airport.sky_id = data['PlaceId']
        airport.country_name = data['CountryName']
        airport.city_name = data['CityName']

        if save:
            try:
                airport.save()
            except IntegrityError:
                pass
            except Exception as ex:
                logger.error("Could not save the airport: {}".format(ex))

        return airport


class Carrier(models.Model):
    sky_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=255)


class Flight(models.Model):
    airport_start = models.ForeignKey(Airport, default=None, to_field='sky_id', related_name='%(class)s_airport_start')
    airport_end = models.ForeignKey(Airport, default=None, to_field='sky_id', related_name='%(class)s_airport_end')
    start_date = models.DateTimeField()
    return_date = models.DateTimeField()
    last_update = models.DateTimeField(auto_now_add=True)
    price = models.IntegerField()
    currency = models.CharField(max_length=3)
    buy_url = models.TextField()
    perm_url = ShortUUIDField(unique=True)
    carrier = models.ForeignKey(Carrier, default=None, null=True, to_field='sky_id', related_name='%(class)s_carrier')
    clicks = models.IntegerField(default=0)

    def __str__(self):
        return self.perm_url

    class Meta:
        unique_together = (('start_date', 'return_date', 'price', 'currency', 'airport_end', 'airport_start', ),)

    def to_dict(self):
        data = {
            'airport_start': {
                'iata_code': self.airport_start.iata_code,
                'name': self.airport_start.name
            },
            'airport_end': {
                'name': self.airport_end.name,
                'iata_code': self.airport_end.iata_code
            },
            'start_date': self.start_date,
            'return_date': self.return_date,
            'last_update': self.last_update,
            'price': self.price,
            'currency': self.currency,
            'buy_url': self.buy_url,
            'perm_url': self.perm_url
        }

        return data

    @staticmethod
    def save_flight(airport1, airport2, response_data):
        """

        :param airport1:
        :type airport1: Airport
        :param airport2:
        :type airport2: Airport
        :param response_data:
        :type response_data: dict
        :return:
        """

        try:
            carrier = Carrier.objects.get(sky_id=response_data['first_way']['carrier']['id'])
        except Carrier.DoesNotExist:
            carrier = Carrier()
            # we keep only one carrier
            carrier.sky_id = response_data['first_way']['carrier']['id']
            carrier.name = response_data['first_way']['carrier']['name']
            carrier.save()

        flight = Flight()
        flight.airport_start_id = airport1.sky_id
        flight.airport_end_id = airport2.sky_id
        flight.price = response_data['price']
        flight.buy_url = response_data['buyUrl']
        flight.currency = settings.CURRENCY
        flight.start_date = datetime.strptime(response_data['first_way']['date'], '%d-%m-%Y')
        flight.return_date = datetime.strptime(response_data['return']['date'], '%d-%m-%Y')
        flight.carrier = carrier

        try:
            flight.save()
        except IntegrityError:
            logger.error("Flight already exists. Retrieve...")
            flight = Flight.objects.get(start_date=flight.start_date,
                                        return_date=flight.return_date,
                                        price=flight.price,
                                        currency=flight.currency,
                                        airport_end_id=flight.airport_end_id,
                                        airport_start_id=flight.airport_start_id)
        except Exception as e:
            logger.error("Could not save the flight to DB: {}, {}".format(e, flight.__dict__))

        return flight


class Tag(models.Model):
    tag = models.CharField(max_length=255)

    def __str__(self):
        return self.tag


class Article(models.Model):

    STATUS_PUBLISHED = 'PUB'
    STATUS_DRAFT = 'DRAFT'
    STATUS_DELETED = 'DEL'
    STATUSES = (
        (STATUS_PUBLISHED, 'Published'),
        (STATUS_DELETED, 'Deleted'),
        (STATUS_DRAFT, 'Draft')
    )

    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=255, editable=False, default='',
                            unique=True,)
    content = models.TextField()
    status = models.CharField(max_length=10,
                              choices=STATUSES,
                              default=STATUS_DRAFT,)
    accept_comments = models.BooleanField()
    published_on = models.DateTimeField()
    locale = models.CharField(max_length=10, default='en-US', choices=settings.LANGUAGES)  # en-US
    tags = models.ManyToManyField(Tag, related_name='tags')

    def is_published(self):
        return self.status == self.STATUS_PUBLISHED

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(Article, self).save(*args, **kwargs)
