import json
import logging
import requests
from dateutil import parser

from django.urls import reverse
from django.conf import settings
from django.utils.translation import get_language
from django.core.cache import cache

from dealflight.utils.date import get_start_end_dates
from dealflight.utils.crypto import md5
from dealflight.exceptions import NoQuoteException
from .models import Airport, Flight

logger = logging.getLogger('flights')


def compose_skyscanner_url(geo, guess_city=False):
    market = geo['iso_code']
    currency = settings.CURRENCY
    locale = get_language()
    destination = 'anywhere'

    source = "{ip}-ip".format(ip=geo['ip'])
    if guess_city:
        source = geo['iso_code']

    start_date, end_date = get_start_end_dates()

    main_url = "{}/{}/{}/{}/{}/{}/{}/{}/{}/{}".format(settings.SKYSCANNER_BASE_URL,
                                                      'browsequotes',
                                                      settings.SKYSCANNER_VERSION,
                                                      market, currency, locale, source, destination,
                                                      start_date, end_date)

    url = "{main_url}?apiKey={api_key}".format(main_url=main_url, api_key=settings.SKYSCANNER_KEY)

    return url.encode('utf8')


def get_carriers(carriers, quote):
    logger.error(carriers)
    logger.error(quote)
    if quote['Direct'] is False:
        logger.error("No direct flight. Multiple carriers possible")

    carrier_id1 = ''
    carrier_id2 = ''
    try:
        carrier_id1 = quote['InboundLeg']['CarrierIds'][0]
    except IndexError:
        pass
    try:
        carrier_id2 = quote['OutboundLeg']['CarrierIds'][0]
    except IndexError:
        pass

    carrier_name1 = carrier_name2 = ''
    for carrier in carriers:
        if carrier['CarrierId'] == carrier_id1:
            carrier_name1 = carrier['Name']

        if carrier['CarrierId'] == carrier_id2:
            carrier_name2 = carrier['Name']

        if carrier_name1 != '' and carrier_name2 != '':
            break

    c1 = {
        'id': carrier_id1,
        'name': carrier_name1
    }

    c2 = {
        'id': carrier_id2,
        'name': carrier_name2
    }

    return c1, c2, quote['Direct']


def get_airport(places, place_id):
    """
    Extract airport from SK respoonse

    :param places:
    :param place_id:
    :return: dict
    """
    for place in places:
        if place['PlaceId'] == place_id:
            return place
    return {}


def get_min_quote(quotes, exceptAirports=None):
    """
    Returns the minimum quote from a list of quotes

    :param quotes:
    :param exceptAirports:
    :return: a quote dict
    :rtype: dict
    """
    if not len(quotes):
        raise NoQuoteException()

    if not exceptAirports:
        exceptAirports = []

    validQuotes = []
    # logger.error("quotes={}".format(quotes))
    for quote in quotes:
        if quote['OutboundLeg']['DestinationId'] not in exceptAirports:
            validQuotes.append(quote)

    quote_min = {}
    if len(validQuotes):
        quote_min = validQuotes[0]

    for quote in validQuotes:
        if quote['MinPrice'] < quote_min['MinPrice']:
            quote_min = quote

    return quote_min


def get_ids_by_country(countries, places):
    airports = []
    for place in places:
        if place.get('CountryName', '') in countries:
            airports.append(place['PlaceId'])
    return airports


def get_response_data(data, currency):
    # TODO - skip airports from the same country
    romanian_airports = get_ids_by_country(countries=('România', 'Romania'),
                                           places=data['Places'])
    quote_min = get_min_quote(quotes=data['Quotes'],
                              exceptAirports=romanian_airports)

    airport_origin_start = get_airport(data['Places'], quote_min['OutboundLeg']['OriginId'])
    airport1 = Airport.from_dict(airport_origin_start, save=True)

    airport_destination_start = get_airport(data['Places'], quote_min['OutboundLeg']['DestinationId'])
    airport2 = Airport.from_dict(airport_destination_start, save=True)

    airport_origin_return = get_airport(data['Places'], quote_min['InboundLeg']['OriginId'])
    airport_destination_return = get_airport(data['Places'], quote_min['InboundLeg']['DestinationId'])

    raw_date = quote_min['OutboundLeg']['DepartureDate']
    dt = parser.parse(raw_date)
    display_date_first_way = "{}-{}-{}".format(str(dt.day).zfill(2),
                                               str(dt.month).zfill(2),
                                               dt.year)

    raw_date = quote_min['InboundLeg']['DepartureDate']
    dt = parser.parse(raw_date)
    display_date_return = "{}-{}-{}".format(str(dt.day).zfill(2),
                                            str(dt.month).zfill(2),
                                            dt.year)

    carrier1, carrier2, direct = get_carriers(data['Carriers'], quote_min)

    response_data = {
        'price': int(quote_min['MinPrice']),
        'currency': currency,
        'direct': direct,
        'first_way': {
            'date': display_date_first_way,
            'from': airport_origin_start['Name'],
            'skCodeFrom': airport_origin_start['SkyscannerCode'],
            'to': airport_destination_start['Name'],
            'skCodeTo': airport_destination_start['SkyscannerCode'],
            'carrier': carrier1,
        },
        'return': {
            'date': display_date_return,
            'from': airport_origin_return['Name'],
            'skCodeFrom': airport_origin_return['SkyscannerCode'],
            'to': airport_destination_return['Name'],
            'skCodeTo': airport_destination_return['SkyscannerCode'],
            'carrier': carrier2,
        }
    }

    return response_data, airport1, airport2


def get_cheapest_flight(geo, guess_city=False, no_cache=False):
    baseurl = settings.SKYSCANNER_BASE_URL
    market = geo['iso_code']
    currency = settings.CURRENCY

    start_date, end_date = get_start_end_dates()

    url = compose_skyscanner_url(geo, guess_city)

    cache_key = md5(url)

    if no_cache is False:
        response_data = cache.get(cache_key, None)

        if response_data:
            logger.debug("Returned data from cache: geo={}, guess_city={}".format(geo, guess_city))
            return json.loads(response_data)

        logger.debug("No data found in cache: geo={}, guess_city={}, url={}".format(geo, guess_city, url))

    logger.debug("Call url: {}".format(url))
    response = requests.get(url)
    if response.status_code == requests.codes.bad_request:
        logger.error("Bad Request to skyscanner: {} {}".format(url, response.content))
        raise NoQuoteException

    if response.status_code != requests.codes.ok:
        logger.error("SKYSCANNER ERROR. Could not get {}".format(url))
        raise NoQuoteException

    data = response.json()

    try:
        response_data, airport1, airport2 = get_response_data(data=data, currency=currency)
    except NoQuoteException:
        logger.error("No quote found for: {}".format(url))
        raise

    locale = get_language()

    referral_url = '{baseurl}/referral/v1.0/{market}/{currency}/{locale}/' \
                   '{firstWaySkCodeFrom}/{firstWaySkCodeTo}/{startDate}/' \
                   '{endDate}?apiKey=' \
                   '{shortApiKey}'.format(baseurl=baseurl,
                                          market=market,
                                          currency=currency,
                                          locale=locale,
                                          firstWaySkCodeFrom=response_data['first_way']['skCodeFrom'],
                                          firstWaySkCodeTo=response_data['first_way']['skCodeTo'],
                                          startDate=start_date, endDate=end_date,
                                          shortApiKey=settings.SKYSCANNER_KEY[:16])

    response_data['buyUrl'] = referral_url

    flight = Flight.save_flight(airport1, airport2, response_data)
    response_data['permUrl'] = reverse('flight', args=[flight.perm_url, ])

    cache.set(cache_key, json.dumps(response_data), settings.CACHE_TTL)

    return response_data
