from django.conf.urls import url
from dealflight.views.index import IndexView
from dealflight.views.next_flights import NextFlightsView
from dealflight.views.article import ArticleView, ArticlesView
from dealflight.views.about import AboutView
from dealflight.views.flight import FlightView
from dealflight.views.city import SearchCityView, CityView
from dealflight.views.sitemap import SitemapView
from dealflight.views.check_price import CheckPriceView


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^next-flights/$', NextFlightsView.as_view(), name='next-flights'),
    url(r'^flight/(?P<perm_url>.+)/$', FlightView.as_view(), name='flight'),
    url(r'^search-city/$', SearchCityView.as_view(), name='search_city'),
    url(r'^city/(?P<city_id>[A-Z]{2,10}-sky)/$', CityView.as_view(), name='city'),
    url(r'^check/(?P<flight_id>.+)/$', CheckPriceView.as_view(), name='check_price'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^sitemap.xml$', SitemapView.as_view(), name='api'),
    url(r'^articles/(?P<page>.*)$', ArticlesView.as_view(), name='articles'),
    url(r'^article/(?P<article_id>(\d+))-(?P<slug>.+)/$', ArticleView.as_view(), name='article'),
]

# TODO
todo_urls = [
    # url(r'^more-flights/(?P<city_id>[A-Z]{2,10}-sky)/$', views.more_flights, name='more_flights'),
    # url(r'^best-flight/$', views.search_index, name='search-index'),
    # url(r'^api-info/$', views.api_info, name='api'),
    # url(r'^confirm/(?P<key>.+)$', views.confirm, name='api'),
    # url(r'^subscribe/$', views.subscribe, name='subscribe'),
]