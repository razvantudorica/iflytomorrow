# iFlyTomorrow

[See it in action](http://iflytomorrow.com)

[JSON response](http://iflytomorrow.com/?json=1)


## How to install
1. virtualenv -p python3 flights_env
2. source flights_env/bin/activate
3. git clone git@bitbucket.org:razvantudorica/flights.git
4. cd flights
5. Add GeoIP Lite database:
```
mkdir geoip
cd geoip
wget http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz 
wget http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz
gunzip GeoLite2-City.mmdb.gz 
gunzip GeoLite2-Country.mmdb.gz
sudo add-apt-repository ppa:maxmind/ppa
sudo apt-get install install libmaxminddb0 libmaxminddb-dev mmdb-bin
cd ..
```
6. pip install -r requirements.txt
7. mkdir django_cache (make it writable for the app)
8. cd flights
9. cp skyscanner_settings.py.template skyscanner_settings.py
10. fill the skyscanner api key in skyscanner_settings.py
11. python3 manage.py collectstatic
12. python3 manage.py migrate
13. python3 manage.py runserver

## MySQL support

```sh
# on mac
brew install mysql-connector-c
```

##How to create translations

```sh
cd dealflight/
```

### To create a new language:
```sh
django-admin.py makemessages -l de
```

### To rescan the files for the current languages:
```sh
django-admin.py makemessages -a -i "*.txt"
```

### To create the mo file:
```sh
django-admin.py compilemessages
```

More info [here](http://www.djangobook.com/en/2.0/chapter19.html).

## Production Deployment

### Create a tag

```sh
# on dev
git tag -a v1.1.2 -m "version 1.1.2"
git push origin --tags
```

```sh
# on prod
git pull origin --tags
git checkout v1.1.2
```
