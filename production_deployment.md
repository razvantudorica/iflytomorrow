### Production Deployment

```
mkdir -p /home/user/project
mkdir -p /home/user/project/public_html
mkdir -p /home/user/project/logs
mkdir -p /home/user/project/env

cd /home/user/project
git clone git@bitbucket.org:razvantudorica/flights.git public_html
virtualenv -p python3 env
source env/bin/activate
cd public_html
pip install -r requirements.txt
pip install gunicorn
# on freebsd
pkg install py27-supervisor
```

Create file:
/home/user/project/env/bin/gunicorn_start
with content:
```
#!/bin/bash

NAME="flights"                                             # Django Project Name
DJANGODIR=/home/user/project/public_html                      # Django Project Directory
SOCKFILE=/home/user/project/gunicorn.sock     # Gunicorn Sock File
USER=razvantudorica                                                # Django Project Running under user vagrant
GROUP=razvantudorica                                                # Django Project Running under group vagrant
NUM_WORKERS=3
DJANGO_SETTINGS_MODULE=flights.settings                     # change 'myproject' with your project name
DJANGO_WSGI_MODULE=flights.wsgi                             # change 'myproject' with your project name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source ../env/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ../env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--log-file=-

```


In /usr/local/etc/supervisord.conf
add on the last line

```
[include]
files = supervisor.d/*.ini
```

```
mkdir /usr/local/etc/supervisor.d/

vim /usr/local/etc/supervisor.d/flights.ini
```

and add:

```
[program:flights]
command = sh /home/user/project/env/bin/gunicorn_start
user = razvantudorica
stdout_logfile = /home/user/project/logs/gunicorn_supervisor.log
redirect_stderr = true
environment=LANG=en_US.UTF-8,LC_ALL=en_US.UTF-8
```

```
sysrc supervisord_enable=yes
service supervisord restart
```

Create nginx file:
/usr/local/etc/nginx/sites/flights.razvantudorica.net.conf
See content in nginx_config_files repo.

```
python manage.py collectstatic
```