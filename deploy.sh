#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <tag>"
    exit 1
fi

tag=$1

git fetch --tags
git checkout $tag
git describe > dealflight/version.txt
