#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <tag>"
    exit 1
fi

tag=$1

git tag -d $tag
git push origin :refs/tags/$tag
